package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.transition.Slide;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity {

    NavigationView nav;
    ActionBarDrawerToggle toggle;
    DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        nav=(NavigationView)findViewById(R.id.navmenu);
        drawerLayout=(DrawerLayout)findViewById(R.id.drawer);

        toggle=new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        getSupportFragmentManager().beginTransaction().replace(R.id.container,new homefragment()).commit();
        nav.setCheckedItem(R.id.menu_home);

        nav.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            Fragment temp;
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem)
            {
                switch (menuItem.getItemId())
                {
                    case R.id.menu_home :
                        temp=new homefragment();
                        break;
                    case R.id.venue :
                        temp=new venuefragment();
                        break;
                    case R.id.equipment :
                        temp=new equipmentfragment();
                        break;
                    case R.id.fitness :
                        temp=new fitnessfragment();
                        break;
                    case R.id.bmi :
                        temp=new bmifragment();
                        break;
                    case R.id.clinic :
                        temp=new clinicfragment();
                        break;
                    case R.id.health :
                        temp=new healthfragment();
                        break;
                    case R.id.drinkWater :
                        temp=new drinkwaterfragment();
                        break;
                    case R.id.notification :
                        temp=new notificationfragment();
                        break;
                    case R.id.profile :
                        temp=new profilefragment();
                        break;
                    case R.id.contact :
                        temp=new contactfragment();
                        break;
                    case R.id.about :
                        temp=new aboutfragment();
                        break;

                }
                getSupportFragmentManager().beginTransaction().replace(R.id.container,temp).commit();
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            }
        });

    }


}