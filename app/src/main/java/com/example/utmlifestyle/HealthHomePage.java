package com.example.utmlifestyle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HealthHomePage extends AppCompatActivity {

    private Button back;
    private Button clinic;
    private Button healthRecord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_home_page);

        clinic = findViewById(R.id.clinic_Appontment);

        clinic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clinic();
            }
        });

        back = findViewById(R.id.back_icon);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Back();
            }
        });

        healthRecord = findViewById(R.id.health_record);
        healthRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                record();
            }
        });




    }

    private void record() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

    private void clinic() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


    public void Back() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

}